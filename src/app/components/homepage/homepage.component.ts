import { Component, HostListener, Inject , OnInit} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import * as AOS from 'aos';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  title = 'pokerface-homepage';

  storedTheme = localStorage.getItem('theme-color');

  constructor(@Inject(DOCUMENT) private document: Document) { }
  ngOnInit(): void {
    AOS.init();
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    
    if (document.body.scrollTop > 520 ||     
    document.documentElement.scrollTop > 520) {
      document.getElementById('advertisement').classList.add('slide-in');
    }
    else{
      document.getElementById('advertisement').classList.remove('slide-in');
    }

    if (document.body.scrollTop > 300 ||     
    document.documentElement.scrollTop > 300) {
      document.getElementById('cb-mb-row-games').classList.add('slide-in');
    }
    else{
      document.getElementById('cb-mb-row-games').classList.remove('slide-in');
    }

    if (document.body.scrollTop > 1500 ||     
      document.documentElement.scrollTop > 1500) {
      document.getElementById('advertisement').classList.add('remove-slide-in');
    }
    else{
      document.getElementById('advertisement').classList.remove('remove-slide-in');
    }
  
  }
  config: SwiperOptions = {
    loop: true,
    loopFillGroupWithBlank: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true
    },
  };  
  
  swiperprovider: SwiperOptions = {
    slidesPerView: 7,
    spaceBetween: 0,
    slidesPerGroup: 1,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  };  

  @HostListener('window:load')
  onLoad() {
    var getThemeColor = document.querySelector(".theme-color");
 
    
    if(this.storedTheme === 'theme-dark') {
      
      localStorage.setItem('theme-color', 'theme-dark');
      this.storedTheme = localStorage.getItem('theme-color');

      getThemeColor.classList.remove("theme-light");
    } 
    if(this.storedTheme === 'theme-light') {

      localStorage.setItem('theme-color', 'theme-light');
      this.storedTheme = localStorage.getItem('theme-color');

      getThemeColor.classList.add("theme-light");

    }
    else{
      localStorage.setItem('theme-color', 'theme-dark');
      this.storedTheme = localStorage.getItem('theme-color');
    }

  }

  setTheme() {
    var getThemeColor = document.querySelector(".theme-color");
    if(this.storedTheme === 'theme-dark') {
      
      localStorage.setItem('theme-color', 'theme-light');
      this.storedTheme = localStorage.getItem('theme-color');

      getThemeColor.classList.add("theme-light");
    } else {
      localStorage.setItem('theme-color', 'theme-dark');
      this.storedTheme = localStorage.getItem('theme-color');

      getThemeColor.classList.remove("theme-light");
    }
  }

  showPopup(){
    var element = document.querySelector(".popup-cb");
    element.classList.add("show-popup");   
  }
  cancelPopup(){
    var element = document.querySelector(".popup-cb");
    element.classList.remove("show-popup");   
  }

  showProfile(){
    var element = document.querySelector(".popup-profile-cb");
    element.classList.add("show-profile");   
  }
  cancelProfile(){
    var element = document.querySelector(".popup-profile-cb");
    element.classList.remove("show-profile");   
  }
  
  showPL(){
    var element = document.querySelector(".popup-pl-cb");
    element.classList.add("show-pl");   
  }
  cancelPL(){
    var element = document.querySelector(".popup-pl-cb");
    element.classList.remove("show-pl");   
  }

  clickEvent(){
    var element = document.querySelector(".cb-container");
    element.classList.add("mystyle");   
  }
  switchSignUp(){
    var signup = document.querySelector(".cb-signup");
    var login = document.querySelector(".cb-login");
    signup.classList.add("to-signup");   
    login.classList.remove("to-login");   
  }
  switchLogin(){
    var signup = document.querySelector(".cb-signup");
    var login = document.querySelector(".cb-login");
    signup.classList.remove("to-signup");   
    login.classList.add("to-login");   
  }
  switchSignUpMobile(){
    var signup = document.querySelector(".cb-signup");
    var login = document.querySelector(".cb-login");
    signup.classList.add("to-signup");   
    login.classList.remove("to-login");   
  }
  switchLoginMobile(){
    var signup = document.querySelector(".cb-signup");
    var login = document.querySelector(".cb-login");
    signup.classList.remove("to-signup");   
    login.classList.add("to-login");   
  }
  showFilter(){
    var element = document.querySelector(".filter-container");
    element.classList.toggle("show-filter-box");   
  }
  showSearch(){
    var element = document.querySelector(".search-box-container");
    element.classList.toggle("show");   
  }
  showSearchWidth(){
    const search = document.querySelector(".cb-search");
    // const icon = document.querySelector(".icon");
    search.classList.toggle("active");

  }

  onKey(event: any) {
    const inputValue = event.target.value;
  }

  onSubmit(event: any) {
    event.preventDefault();
    var getInputValue = event.target.search.value;
    var searchContent = this.document.querySelector('.search-content');
    var tabContent = this.document.querySelector('.cb-tab-content');
    if(getInputValue == ''){
      searchContent.classList.remove("show");
      tabContent.classList.remove("hide");
    }
    if(getInputValue == 'Supernowa'){
      searchContent.classList.add("show");
      tabContent.classList.add("hide");
    }
 }
 removeSearchResult(){
    var searchContent = this.document.querySelector('.search-content');
    var tabContent = this.document.querySelector('.cb-tab-content');
    searchContent.classList.remove("show");
    tabContent.classList.remove("hide");
 }
}

