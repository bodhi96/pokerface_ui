import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShadeeffectsComponent } from './shadeeffects.component';

describe('ShadeeffectsComponent', () => {
  let component: ShadeeffectsComponent;
  let fixture: ComponentFixture<ShadeeffectsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShadeeffectsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShadeeffectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
