import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { SwiperOptions } from 'swiper';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.css']
})
export class ProvidersComponent implements OnInit {

  config: SwiperOptions = {
    slidesPerView: 7,
    spaceBetween: 0,
    slidesPerGroup: 1,
    loop: true,
    loopFillGroupWithBlank: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  };  

  constructor(@Inject(DOCUMENT) private document: Document) { }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (document.body.scrollTop > 300 ||     
    document.documentElement.scrollTop > 300) {
      document.getElementById('cb-ad').classList.add('rotate3d');
    }
    else{
      document.getElementById('cb-ad').classList.remove('rotate3d');
    }
  }
  name = 'Angular';

  ngOnInit(): void {
  }

}



