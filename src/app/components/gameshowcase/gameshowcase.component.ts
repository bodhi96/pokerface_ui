import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gameshowcase',
  templateUrl: './gameshowcase.component.html',
  styleUrls: ['./gameshowcase.component.css']
})
export class GameshowcaseComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  showFilter(){
    var element = document.querySelector(".filter-container");
    element.classList.toggle("show-filter-box");   
  }

  showSupernowa(){
    var rowGames = document.querySelector(".cb-games-layer");
    rowGames.classList.toggle("hide"); 
    var iframeContainer = document.querySelector(".cb-iframe-container");
    iframeContainer.classList.toggle("show");   
  }
}
