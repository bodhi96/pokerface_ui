import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameshowcaseComponent } from './gameshowcase.component';

describe('GameshowcaseComponent', () => {
  let component: GameshowcaseComponent;
  let fixture: ComponentFixture<GameshowcaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameshowcaseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameshowcaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
