import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-supernowa',
  templateUrl: './supernowa.component.html',
  styleUrls: ['./supernowa.component.css']
})
export class SupernowaComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  showFilter(){
    var element = document.querySelector(".filter-container");
    element.classList.toggle("show-filter-box");   
  }

}
