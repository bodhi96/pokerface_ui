import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomebeforeloginComponent } from './homebeforelogin.component';

describe('HomebeforeloginComponent', () => {
  let component: HomebeforeloginComponent;
  let fixture: ComponentFixture<HomebeforeloginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomebeforeloginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomebeforeloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
