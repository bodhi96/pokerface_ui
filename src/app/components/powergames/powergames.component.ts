import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-powergames',
  templateUrl: './powergames.component.html',
  styleUrls: ['./powergames.component.css']
})
export class PowergamesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  showSupernowa(){
    var rowGames = document.querySelector(".cb-row-games");
    rowGames.classList.toggle("hide"); 
    var iframeContainer = document.querySelector(".cb-iframe-container");
    iframeContainer.classList.toggle("show");   
  }
  showFilter(){
    var element = document.querySelector(".filter-container");
    element.classList.toggle("show-filter-box");   
  }
}
