import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PowergamesComponent } from './powergames.component';

describe('PowergamesComponent', () => {
  let component: PowergamesComponent;
  let fixture: ComponentFixture<PowergamesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PowergamesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PowergamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
