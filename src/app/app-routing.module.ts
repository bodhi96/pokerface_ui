import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeafterloginComponent } from './components/homeafterlogin/homeafterlogin.component';
import { HomebeforeloginComponent } from './components/homebeforelogin/homebeforelogin.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';

const routes: Routes = [
  { path: '', component: HomepageComponent },
  { path: 'lobby', component: HomeafterloginComponent },
  { path: 'dummy', component: HomebeforeloginComponent }
  // { path: 'login', component: LoginComponent },
  // { path: 'registration', component: RegistrationComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const ComponentRoute = [ LoginComponent, RegistrationComponent ]
