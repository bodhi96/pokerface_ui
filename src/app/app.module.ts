import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }  from '@angular/forms';

import { AppRoutingModule, ComponentRoute } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { HeaderComponent } from './components/header/header.component';


import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { ProvidersComponent } from './components/providers/providers.component';
import { HomebeforeloginComponent } from './components/homebeforelogin/homebeforelogin.component';
import { HomeafterloginComponent } from './components/homeafterlogin/homeafterlogin.component';
import { ShadeeffectsComponent } from './components/shadeeffects/shadeeffects.component';
import { GameshowcaseComponent } from './components/gameshowcase/gameshowcase.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { PowergamesComponent } from './components/powergames/powergames.component';
import { SupernowaComponent } from './components/supernowa/supernowa.component';
import { SearchcontentComponent } from './components/searchcontent/searchcontent.component';

@NgModule({
  declarations: [
    AppComponent,
    ComponentRoute,
    LoginComponent,
    RegistrationComponent,
    HeaderComponent,
    ProvidersComponent,
    HomebeforeloginComponent,
    HomeafterloginComponent,
    ShadeeffectsComponent,
    GameshowcaseComponent,
    HomepageComponent,
    PowergamesComponent,
    SupernowaComponent,
    SearchcontentComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    NgxUsefulSwiperModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
